# Fedora Linux introduction and Web-UI installation guide

The guide is to be released with the Fedora Web-UI guide. Drafts of the guide might be published with unofficial Web-UI previews to facilitate testing of and feedback about the Web-UI tool.  

## Approach drafting

https://gitlab.com/py0xc31/fedora-linux-intro-and-web-ui-installation-guide/-/blob/main/modules/ROOT/pages/WebUI_Install_Guide.adoc  
 -> to be complemented maybe by related pages if they fit the overall guide concept

## Local preview

### Installing Podman and git on Fedora

Fedora Workstation doesn't come with Podman preinstalled by default — so you might need to install it using the following command:

```
$ sudo dnf install podman
```

### Preview as a part of the whole Fedora Docs site

You can also build the whole Fedora Docs site locally to see your changes in the whole context.
This is especially useful for checking if your `xref` links work properly.

Steps:

Clone the main repository and cd into it:

```
$ git clone https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide
$ cd fedora-linux-workstation-spins-guide
```

To build the whole site, I would run the following in the `fedora-linux-workstation-spins-guide` directory.

```
$ ./build.sh && ./preview.sh
```

The result will be available at http://localhost:8080  

You can use the following links to directly get to the respective preview of the drafts mentioned above:  
 - [Index with approach of the guide](http://localhost:8080/pizza-factory/)  
 - [Page example: BackUp](http://localhost:8080/pizza-factory/BackUp/) (Important tasks and automation -> BackUp, snapshots and RAID)  
 - The "Web UI Installation Guide" page currently only drafts the approach and tracks the development and status. It contains no content that is intended for presentation on Docs pages: at the menu entry "Choose and install Workstation or spins", it will be replaced once a draft for the guide is in development.  

# License

SPDX-License-Identifier: CC-BY-SA-4.0
